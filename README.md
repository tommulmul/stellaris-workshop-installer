# Stellaris workshop installer

A tool that pulls mods or collections from the workshop and installs them in your stellaris folder

Can automatically add mod profiles for [**Paradoxos Mod Manager**](https://github.com/ThibautSF/ParadoxosModManagerRework/). 
The mod order will be set the same as it is on the steam workshop

## Download

Get the latest version: [**here**](https://gitlab.com/tommulmul/stellaris-workshop-installer/-/releases)

## Installation

### **Prerequisites**
* [**Python 3**](https://www.python.org/downloads/)
* [**pip**](https://pip.pypa.io/en/stable/installing/) *Included with default python installation*
* The rest of the requirements can be easily installed using pip by opening a command shell in the install directory and typing ```pip install -r requirements.txt```

### **Usage**
`python download.py [mod/collection id] (--install_path C:\...) (--paradoxos_path C:\...) (--help)`

* The mod/collection id refers to the steam workshop id number: https:/<span>/steamcommunity.com/sharedfiles/filedetails/?id=**819148835**
* Only the mod/collection id is required, the install path defaults to `C:\User\[username]\Documents\Paradox Interactive\Stellaris\mod`
* `--paradoxos-path` is required if you want to have the tool automatically add a mod collection profile to paradoxos for collections
* `--help` prints a reminder for the tool arguments
