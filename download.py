import argparse
import json
import aiohttp
import re
import asyncio
from pathlib import Path
from tqdm import tqdm
import zipfile
import shutil
from slugify import slugify
import os

parser = argparse.ArgumentParser()
parser.add_argument("target")
parser.add_argument("--install_path", "-ip", nargs=1, default=["~/Documents/Paradox Interactive/Stellaris/mod"])
parser.add_argument("--chunk_size", nargs=1, type=int, default=[4096])
parser.add_argument("--paradoxos_path", nargs=1)


async def install_workshop_content(target):
    async with aiohttp.ClientSession() as session:
        response = await get_collection_details(session, target)
        if response["resultcount"] == 0:
            # single item
            await install_workshop_file(session, response["collectiondetails"][0]["publishedfileid"])
        else:
            await install_workshop_collection(session, response["collectiondetails"][0])


async def install_workshop_file(session, file_id, progressbar_position=None):
    file_details = await get_file_details(session, file_id)
    file_title = slugify(file_details["publishedfiledetails"][0]["title"])
    file = await download_file_workshop(session, file_id, file_title, progressbar_position)

    with zipfile.ZipFile(file) as zip_file:
        zip_file.extractall(install_location)
    file.unlink(missing_ok=True)

    mod_folder = Path(install_location, file_title)
    dl_folder = Path(install_location, file_id)
    shutil.copytree(str(dl_folder.resolve()), str(mod_folder.resolve()), dirs_exist_ok=True)
    shutil.rmtree(str(dl_folder.resolve()))

    contained_zips = mod_folder.glob("*.zip")
    if contained_zips:
        for contained_zip in contained_zips:
            with zipfile.ZipFile(contained_zip) as zip_file:
                zip_file.extractall(install_location / file_title)
            contained_zip.unlink(missing_ok=True)

    descriptor_file_internal = Path(mod_folder, "descriptor.mod")
    descriptor_file_external = Path(install_location, file_title + ".mod")
    with descriptor_file_internal.open("a") as descriptor:
        descriptor.write("\npath=\"" + str(mod_folder.resolve()) + "\"")
    shutil.copy(str(descriptor_file_internal.resolve()), str(descriptor_file_external.resolve()))
    return {"descriptor": descriptor_file_external,
            "title": file_title,
            "fileid": file_id}


def build_paradoxos_profile(collection_item):
    pass


async def install_workshop_collection(session, collection):
    descriptors = dict((item["fileid"], item) for item in await asyncio.gather(*map(
        lambda collection_item: install_workshop_file(session, collection_item["publishedfileid"],
                                                      collection_item["sortorder"]),
        collection["children"])))
    if args.paradoxos_path:
        collection_details = (await get_file_details(session, collection["publishedfileid"]))["publishedfiledetails"][0]
        collection_title = collection_details["title"]
        collection_description = collection_details["description"]
        userlists = Path(args.paradoxos_path[0]) / "settings" / "Stellaris" / "UserLists.json"
        mods = []
        for i, collection_item in enumerate(collection["children"]):
            file_id = collection_item["publishedfileid"]
            descriptor = descriptors[file_id]
            mods.append({
                "modName": descriptor["title"],
                "fileName": descriptor["descriptor"].name,
                "remoteID": file_id,
                "order": i
            })
        with userlists.open("r") as userlists_file:
            userlists_json = json.load(userlists_file)
            userlists_json["userlists"].append({"name": collection_title, "descr": collection_description
                                                   , "customOrder": True, "launchargs": "", "lang": "english"
                                                   , "mod": mods})
        with userlists.open("w") as userlists_file:
            json.dump(userlists_json, userlists_file)


async def get_collection_details(session, collection_id):
    async with session.post("http://api.steampowered.com/ISteamRemoteStorage/GetCollectionDetails/v1/"
            , data={"collectioncount": 1, "publishedfileids[0]": collection_id, "format": "json"}) as response:
        assert response.status == 200
        return json.loads(await response.text())["response"]


async def get_file_details(session, file_id):
    async with session.post("https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1"
            , data={"itemcount": 1, "publishedfileids[0]": file_id, "format": "json"}) as response:
        assert response.status == 200
        return json.loads(await response.text())["response"]


async def download_file_workshop(session, file_id, progressbar_description=None, progressbar_position=None):
    await session.get("http://steamworkshop.download/download/view/" + file_id)
    resp = await (await session.post("http://steamworkshop.download/online/steamonline.php",
                                     data={"item": file_id, "app": "281990"})).text()
    file_url = re.search("(?:<a href=')(.*?)(?:'>)", resp).group(1)
    path = install_location / (file_id + ".zip")
    with path.open("wb+") as physical_file:
        async with session.get(file_url) as resp:
            with tqdm(total=resp.content_length, ascii=True, position=None) as t:
                t.set_description(progressbar_description)
                while True:
                    chunk = await resp.content.read(args.chunk_size[0])
                    if not chunk:
                        break
                    physical_file.write(chunk)
                    t.update(len(chunk))
    return path


if __name__ == "__main__":
    args = parser.parse_args()
    install_location = Path(args.install_path[0]).expanduser()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(install_workshop_content(args.target))
    os.system('cls' if os.name == 'nt' else 'clear')
